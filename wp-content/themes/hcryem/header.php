<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( ' | ', true, 'right' ); ?></title>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/swiper.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/style.css">
<?php wp_head(); ?>
</head>
<body>

<header>
	<div class="container">
		<div class="logo"></div>
		<div class="nav-bar">
			<ul>
				<li>Home</li>
				<li>About</li>
				<li>Service</li>
				<li>Contact</li>
			</ul>
		</div>
	</div>
</header>