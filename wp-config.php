<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hcryem');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$h&-gHN(!|yUBx>9hZf1g7iJ,NU2xe( DoK5pi3jV4t<kcCfLosG[gc;R/k$g8Ew');
define('SECURE_AUTH_KEY',  'd5&;GJ[<?y#KOR|epAei.-?*G10izJplNf/YN7-H4!EX&mbz:]vi[Ry{ [NTU4~)');
define('LOGGED_IN_KEY',    '33DPFzD}yYtCp>?72/cw]axIhat^ N/Dya(KB},OUxjN?^|tIJ]^zbhBFAzxl&ld');
define('NONCE_KEY',        'QMUTEL,Y$;2bl2VJ}{T:Z|yQQu;p9B[SX4x|^>Y#BU,6JL+ER|}!ccC%Bq-Z+r:6');
define('AUTH_SALT',        '0w+C=ov@Y{qrb9gm(UeH|&2@(k:-/Ia9m*S;l@H^lfhCpyyU)l]`I,M8knO|#mcX');
define('SECURE_AUTH_SALT', 'sOG&Wv`_>/6-2%v{N^E]jjBk#QgD:+O]bH,|@eeL{[/aoX&~s* @#EJ_S.,~dv3A');
define('LOGGED_IN_SALT',   '-1CGA|el7X1rVkzw0/~(``s+K,gh)C_E}-!l.sYc-77Y-5;vO3M6HLa4so#0rFtw');
define('NONCE_SALT',       'u+c#x}2=Db!n+6dNT-2&#$Dt,U#q9+Wb*34@(3rtb*Ytn4k-hPl+:POC]zGYg1ts');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
